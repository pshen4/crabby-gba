use crate::primitives::RegisterNum;
use num_derive::FromPrimitive;
use num_traits::FromPrimitive;
use util::bits::GetBits;

#[derive(Debug, Clone, Copy, PartialEq, Eq, FromPrimitive)]
pub enum OpImmediateOpCode {
    MOV = 0,
    CMP = 1,
    ADD = 2,
    SUB = 3,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct OpImmediate {
    op: OpImmediateOpCode,
    dest: RegisterNum,
    offset: u8,
}

impl From<u16> for OpImmediate {
    fn from(raw: u16) -> Self {
        debug_assert!(raw.get_bits::<u16, 15, 13>() == 0b001);
        OpImmediate {
            op: FromPrimitive::from_u16(raw.get_bits::<u16, 12, 11>()).unwrap(),
            dest: RegisterNum(raw.get_bits::<u8, 10, 8>()),
            offset: raw.get_bits::<u8, 7, 0>(),
        }
    }
}

#[cfg(test)]
mod test {
    use super::{OpImmediate, OpImmediateOpCode};
    use crate::primitives::RegisterNum;
    use test_case::test_case;

    #[test_case(OpImmediate {
        op: OpImmediateOpCode::MOV,
        dest: RegisterNum(0b101),
        offset: 0b00010001
    }, 0b001_00_101_00010001)]
    #[test_case(OpImmediate {
        op: OpImmediateOpCode::CMP,
        dest: RegisterNum(0b101),
        offset: 0b00010001
    }, 0b001_01_101_00010001)]
    #[test_case(OpImmediate {
        op: OpImmediateOpCode::ADD,
        dest: RegisterNum(0b000),
        offset: 0b00000000
    }, 0b001_10_000_00000000)]
    #[test_case(OpImmediate {
        op: OpImmediateOpCode::SUB,
        dest: RegisterNum(0b001),
        offset: 0b11111110
    }, 0b001_11_001_11111110)]
    fn opcode(opcode: OpImmediate, binary: u16) {
        assert_eq!(opcode, OpImmediate::from(binary));
    }

    use proptest::prelude::*;
    proptest! {
        #[test]
        fn values_match(
            dest in 0..(1 << 3) as u16,
            offset in 0..(1 << 8) as u16,
        ) {
            let op_immediate = OpImmediate::from((1 << 13) | (dest << 8) | offset);
            prop_assert_eq!(op_immediate.offset, offset as u8);
            prop_assert_eq!(op_immediate.dest.0, dest as u8);
        }
    }
}
