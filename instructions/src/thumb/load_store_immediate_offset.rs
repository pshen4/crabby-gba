use num_traits::FromPrimitive;
use util::bits::GetBits;

use crate::primitives::{ByteWord, LoadStore, RegisterNum};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct LoadStoreImmediateOffset {
    transfer: ByteWord,
    mode: LoadStore,
    offset5: u8,
    base: RegisterNum,
    dest: RegisterNum,
}

impl From<u16> for LoadStoreImmediateOffset {
    fn from(raw: u16) -> Self {
        debug_assert!(raw.get_bits::<u16, 15, 13>() == 0b011);

        Self {
            transfer: FromPrimitive::from_u8(raw.get_bit::<12>()).unwrap(),
            mode: FromPrimitive::from_u8(raw.get_bit::<11>()).unwrap(),
            offset5: raw.get_bits::<u8, 10, 6>(),
            base: RegisterNum(raw.get_bits::<u8, 5, 3>()),
            dest: RegisterNum(raw.get_bits::<u8, 2, 0>()),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::{
        primitives::{ByteWord, LoadStore, RegisterNum},
        thumb::LoadStoreImmediateOffset,
    };
    use test_case::test_case;

    #[test_case(0b011_0_000000000000, ByteWord::Word ; "Word")]
    #[test_case(0b011_1_000000000000, ByteWord::Byte ; "Byte")]
    fn transfer(input: u16, expected: ByteWord) {
        assert_eq!(expected, LoadStoreImmediateOffset::from(input).transfer)
    }

    #[test_case(0b0110_0000_0000_0000, LoadStore::Store ; "Store")]
    #[test_case(0b0110_1000_0000_0000, LoadStore::Load  ; "Load")]

    fn mode(input: u16, expected: LoadStore) {
        assert_eq!(expected, LoadStoreImmediateOffset::from(input).mode)
    }

    use proptest::prelude::*;

    proptest! {
        #[test]
        fn props(
            offset in 0..0b11111_u16,
            base in 0..0b111_u16,
            dest in 0..0b111_u16
        ) {
            let input  = (0b011 << 13) | (offset << 6) | (base << 3) | dest;
            let actual = LoadStoreImmediateOffset::from(input);

            prop_assert_eq!(offset, actual.offset5 as u16);
            prop_assert_eq!(RegisterNum(base as u8), actual.base);
            prop_assert_eq!(RegisterNum(dest as u8), actual.dest);
        }
    }
}
