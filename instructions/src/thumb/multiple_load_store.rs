use num_traits::FromPrimitive;
use util::bits::GetBits;

use crate::primitives::{LoadStore, RegisterNum};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct MultipleLoadStore {
    mode: LoadStore,
    base: RegisterNum,
    rlist: u8,
}

impl From<u16> for MultipleLoadStore {
    fn from(raw: u16) -> Self {
        debug_assert!(raw.get_bits::<u8, 15, 12>() == 0b1100);

        Self {
            mode: FromPrimitive::from_u8(raw.get_bit::<11>()).unwrap(),
            base: RegisterNum(raw.get_bits::<u8, 10, 8>()),
            rlist: raw.get_bits::<u8, 7, 0>(),
        }
    }
}

#[cfg(test)]
mod test {
    use test_case::test_case;

    use crate::{primitives::LoadStore, thumb::MultipleLoadStore};

    #[test_case(0b1100, 0, LoadStore::Store ; "STORE")]
    #[test_case(0b1100, 1, LoadStore::Load  ;  "LOAD")]
    fn mode(input: u16, bit: u16, expected: LoadStore) {
        assert_eq!(
            expected,
            MultipleLoadStore::from((input << 12) | (bit << 11)).mode
        );
    }

    use proptest::prelude::*;

    proptest! {
        #[test]
        fn props(
            base in 0b0..(u8::MAX >> 5) as u16,
            rlist in 0b0..u8::MAX as u16
        ) {
            let input = (0b1100 << 12) | (base << 8) | rlist;
            let actual = MultipleLoadStore::from(input);

            prop_assert_eq!(base, actual.base.0 as u16);
            prop_assert_eq!(rlist, actual.rlist as u16)
        }
    }
}
