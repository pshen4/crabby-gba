use num_traits::FromPrimitive;
use util::bits::GetBits;

use crate::primitives::HighLow;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct LongBranchWithLink {
    hl: HighLow,
    offset: u16,
}

impl From<u16> for LongBranchWithLink {
    fn from(raw: u16) -> Self {
        debug_assert!(raw.get_bits::<u8, 15, 12>() == 0b1111);

        Self {
            hl: FromPrimitive::from_u8(raw.get_bit::<11>()).unwrap(),
            offset: raw.get_bits::<u16, 10, 0>(),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::{primitives::HighLow, thumb::LongBranchWithLink};
    use test_case::test_case;

    #[test_case(0b1111_0000_0000_0000, HighLow::High ; "High")]
    #[test_case(0b1111_1000_0000_0000, HighLow::Low  ; "Low")]
    fn hl(input: u16, expected: HighLow) {
        assert_eq!(expected, LongBranchWithLink::from(input).hl);
    }

    use proptest::prelude::*;

    proptest! {
        #[test]
        fn offset(offset in 0..(u16::MAX >> 5)) {
            let base = (0b1111 << 12) | offset;

            prop_assert_eq!(offset, LongBranchWithLink::from(base).offset);
        }
    }
}
