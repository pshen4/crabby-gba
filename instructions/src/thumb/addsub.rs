use crate::primitives::{RegisterNum, RegisterOrImmediate};
use util::bits::GetBits;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum AddSubtractOpCode {
    ADD,
    SUB,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct AddSubtract {
    op: AddSubtractOpCode,
    roi: RegisterOrImmediate,
    src: RegisterNum,
    dest: RegisterNum,
}

impl From<u16> for AddSubtract {
    fn from(raw: u16) -> Self {
        debug_assert!(raw.get_bits::<u16, 15, 11>() == 0b00011);

        AddSubtract {
            op: AddSubtract::parse_opcode(raw),
            roi: AddSubtract::parse_roi(raw),
            src: RegisterNum(raw.get_bits::<u8, 5, 3>()),
            dest: RegisterNum(raw.get_bits::<u8, 2, 0>()),
        }
    }
}

impl AddSubtract {
    fn parse_roi(raw: u16) -> RegisterOrImmediate {
        match (raw >> 10) & 1 {
            0 => RegisterOrImmediate::Register(RegisterNum(raw.get_bits::<u8, 8, 6>())),
            1 => RegisterOrImmediate::Immediate(raw.get_bits::<u8, 8, 6>()),
            _ => unreachable!(),
        }
    }

    fn parse_opcode(raw: u16) -> AddSubtractOpCode {
        let value = raw.get_bit::<9>();
        match value {
            0 => AddSubtractOpCode::ADD,
            1 => AddSubtractOpCode::SUB,
            _ => unreachable!(),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::primitives::{RegisterNum, RegisterOrImmediate};
    use crate::thumb::addsub::{AddSubtract, AddSubtractOpCode};

    #[test]
    fn op_0_add() {
        let expected = AddSubtractOpCode::ADD;
        let actual = AddSubtract::from(0b0001100000000000);

        assert_eq!(expected, actual.op);
    }

    #[test]
    fn op_1_sub() {
        let expected = AddSubtractOpCode::SUB;
        let actual = AddSubtract::from(0b0001_1010_0000_0000);

        assert_eq!(expected, actual.op);
    }

    #[test]
    fn i_0_register() {
        let expected = RegisterNum(0b111);
        let actual = AddSubtract::from(0b0001_1001_1100_0000).roi;

        assert_eq!(RegisterOrImmediate::Register(expected), actual)
    }

    #[test]
    fn i_1_immediate() {
        let expected = 0b111;
        let actual = AddSubtract::from(0b0001_1101_1100_0000).roi;

        assert_eq!(RegisterOrImmediate::Immediate(expected), actual);
    }

    #[test]
    fn rs() {
        let mut expected = RegisterNum(0b010);
        let mut actual = AddSubtract::from(0b0001_1000_0001_0000);

        assert_eq!(expected, actual.src);

        expected = RegisterNum(0b111);
        actual = AddSubtract::from(0b0001_1000_0011_1000);

        assert_eq!(expected, actual.src);

        expected = RegisterNum(0b001);
        actual = AddSubtract::from(0b0001_1000_0000_1000);

        assert_eq!(expected, actual.src);
    }

    #[test]
    fn rd() {
        let mut expected = RegisterNum(0b001);
        let mut actual = AddSubtract::from(0b0001_1000_0000_0001);

        assert_eq!(expected, actual.dest);

        expected = RegisterNum(0b010);
        actual = AddSubtract::from(0b0001_1000_0000_0010);

        assert_eq!(expected, actual.dest);

        expected = RegisterNum(0b100);
        actual = AddSubtract::from(0b0001_1000_0000_0100);

        assert_eq!(expected, actual.dest);
    }
}
