use crate::primitives::RegisterNum;
use num_derive::FromPrimitive;
use num_traits::FromPrimitive;
use util::bits::GetBits;

#[derive(Debug, Clone, Copy, PartialEq, Eq, FromPrimitive)]
enum MoveShiftedRegisterOpCode {
    LSL = 0,
    LSR = 1,
    ASR = 2,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct MoveShiftedRegister {
    op: MoveShiftedRegisterOpCode,
    offset: u8,
    src: RegisterNum,
    dest: RegisterNum,
}

impl From<u16> for MoveShiftedRegister {
    fn from(raw: u16) -> Self {
        debug_assert_eq!(raw.get_bits::<u16, 15, 13>(), 0);
        debug_assert_ne!(raw.get_bits::<u16, 12, 11>(), 0b11);
        MoveShiftedRegister {
            op: FromPrimitive::from_u16(raw.get_bits::<u16, 12, 11>()).unwrap(),
            offset: raw.get_bits::<u8, 10, 6>(),
            src: RegisterNum(raw.get_bits::<u8, 5, 3>()),
            dest: RegisterNum(raw.get_bits::<u8, 2, 0>()),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::primitives::RegisterNum;
    use crate::thumb::msr::{MoveShiftedRegister, MoveShiftedRegisterOpCode};
    use test_case::test_case;

    #[test_case(MoveShiftedRegister {
        op: MoveShiftedRegisterOpCode::LSL,
        offset: 0b11001,
        src: RegisterNum(0b100),
        dest: RegisterNum(0b011)
    }, 0b000_00_11001_100_011)]
    #[test_case(MoveShiftedRegister {
        op: MoveShiftedRegisterOpCode::LSR,
        offset: 0b10000,
        src: RegisterNum(0b001),
        dest: RegisterNum(0b111)
    }, 0b000_01_10000_001_111)]
    #[test_case(MoveShiftedRegister {
        op: MoveShiftedRegisterOpCode::ASR,
        offset: 0b11001,
        src: RegisterNum(0b100),
        dest: RegisterNum(0b011)
    }, 0b000_10_11001_100_011)]
    fn opcode(msr: MoveShiftedRegister, binary: u16) {
        assert_eq!(msr, MoveShiftedRegister::from(binary));
    }

    use proptest::prelude::*;
    proptest! {
        #[test]
        fn values_match(
            offset in 0..(1 << 5) as u16,
            src in 0..(1 << 3) as u16,
            dest in 0..(1 << 3) as u16
        ) {
            let msr = MoveShiftedRegister::from((offset << 6) | (src << 3) | dest);
            prop_assert_eq!(msr.offset, offset as u8);
            prop_assert_eq!(msr.src.0, src as u8);
            prop_assert_eq!(msr.dest.0, dest as u8);
        }
    }
}
