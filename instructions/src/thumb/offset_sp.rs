use num_traits::FromPrimitive;
use util::bits::GetBits;

use crate::primitives::Sign;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct OffsetStackPointer {
    sign: Sign,
    sword7: u8,
}

impl From<u16> for OffsetStackPointer {
    fn from(raw: u16) -> Self {
        debug_assert!(raw.get_bits::<u16, 15, 8>() == 0b10110000);

        Self {
            sign: FromPrimitive::from_u8(raw.get_bit::<7>()).unwrap(),
            sword7: raw.get_bits::<u8, 6, 0>(),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::{primitives::Sign, thumb::OffsetStackPointer};
    use test_case::test_case;

    #[test_case(0b1011_0000_0000_0000, Sign::Positive ; "Positive")]
    #[test_case(0b1011_0000_1000_0000, Sign::Negative ; "Negative")]
    fn sign(input: u16, expected: Sign) {
        assert_eq!(expected, OffsetStackPointer::from(input).sign);
    }

    use proptest::prelude::*;

    proptest! {
        #[test]
        fn sword7(sword7 in 0..(u8::MAX >> 1) as u16) {
            let actual = OffsetStackPointer::from((0b1011 << 12) | sword7);

            prop_assert_eq!(sword7, From::from(actual.sword7));
        }
    }
}
