use util::bits::GetBits;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct SoftwareInterrupt {
    value: u8,
}

impl From<u16> for SoftwareInterrupt {
    fn from(raw: u16) -> Self {
        debug_assert!(raw.get_bits::<u8, 15, 8>() == 0b11011111);

        Self {
            value: raw.get_bits::<u8, 7, 0>(),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::thumb::SoftwareInterrupt;
    use proptest::prelude::*;

    proptest! {
        #[test]
        fn value(
            value in 0..u8::MAX
        ) {
            let actual = SoftwareInterrupt::from((0b11011111 << 8) | value as u16);

            prop_assert_eq!(value, actual.value);
        }
    }
}
