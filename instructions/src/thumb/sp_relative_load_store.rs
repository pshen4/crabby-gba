use crate::primitives::{LoadStore, RegisterNum};
use num_traits::FromPrimitive;
use util::bits::GetBits;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct SpRelativeLoadStore {
    mode: LoadStore,
    dest: RegisterNum,
    word8: u8,
}

impl From<u16> for SpRelativeLoadStore {
    fn from(raw: u16) -> Self {
        debug_assert!(raw.get_bits::<u16, 15, 12>() == 0b1001);

        Self {
            mode: FromPrimitive::from_u8(raw.get_bit::<11>()).unwrap(),
            dest: RegisterNum(raw.get_bits::<u8, 10, 8>()),
            word8: raw.get_bits::<u8, 7, 0>(),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::{primitives::LoadStore, thumb::sp_relative_load_store::SpRelativeLoadStore};
    use test_case::test_case;

    #[test_case(0b1001, 0, LoadStore::Store ; "STORE")]
    #[test_case(0b1001, 1, LoadStore::Load  ;  "LOAD")]
    fn mode(template: u16, value: u16, expected: LoadStore) {
        assert_eq!(
            expected,
            SpRelativeLoadStore::from((template << 12) | (value << 11)).mode
        )
    }

    use proptest::prelude::*;

    proptest! {
        #[test]
        fn props(
            dest in 0..0b111 as u16,
            word8 in 0..u8::MAX as u16
        ) {
            let base = (0b1001 << 12) | (dest << 8) | word8 as u16;
            assert_eq!(dest as u8, SpRelativeLoadStore::from(base).dest.0);
            assert_eq!(word8 as u8, SpRelativeLoadStore::from(base).word8);
        }
    }
}
