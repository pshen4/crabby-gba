use crate::primitives::{ByteWord, LoadStore, RegisterNum};
use num_traits::FromPrimitive;
use util::bits::GetBits;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct LoadStoreReg {
    bw: ByteWord,
    ls: LoadStore,
    offset: RegisterNum,
    base: RegisterNum,
    target: RegisterNum,
}

impl From<u16> for LoadStoreReg {
    fn from(raw: u16) -> Self {
        debug_assert!(raw.get_bit::<9>() == 0);
        debug_assert!(raw.get_bits::<u16, 15, 12>() == 0b0101);
        Self {
            target: RegisterNum(raw.get_bits::<u8, 2, 0>()),
            base: RegisterNum(raw.get_bits::<u8, 5, 3>()),
            offset: RegisterNum(raw.get_bits::<u8, 8, 6>()),
            ls: FromPrimitive::from_u8(raw.get_bit::<11>()).unwrap(),
            bw: FromPrimitive::from_u8(raw.get_bit::<10>()).unwrap(),
        }
    }
}

#[cfg(test)]
mod test {
    use super::LoadStoreReg;
    use crate::primitives::{ByteWord, LoadStore, RegisterNum};
    use test_case::test_case;

    #[test_case(ByteWord::Byte, 1)]
    #[test_case(ByteWord::Word, 0)]
    fn byteword(bw: ByteWord, bit: u16) {
        let lsr = LoadStoreReg::from(0b0101_0_0_0_000_000_000 | bit << 10);
        assert_eq!(lsr.bw, bw);
    }

    #[test_case(LoadStore::Load, 1)]
    #[test_case(LoadStore::Store, 0)]
    fn loadstore(ls: LoadStore, bit: u16) {
        let lsr = LoadStoreReg::from(0b0101_0_0_0_000_000_000 | bit << 11);
        assert_eq!(lsr.ls, ls);
    }

    use proptest::prelude::*;
    proptest! {
        #[test]
        fn values_match(
            offset in 0..(1 << 3) as u16,
            base in 0..(1 << 3) as u16,
            target in 0..(1 << 3) as u16,
        ) {
            let lsr = LoadStoreReg::from(0b0101_0_0_0_000_000_000 | (offset << 6) | (base << 3) | (target));
            prop_assert_eq!(lsr.offset, RegisterNum(offset as u8));
            prop_assert_eq!(lsr.base, RegisterNum(base as u8));
            prop_assert_eq!(lsr.target, RegisterNum(target as u8));
        }
    }
}
