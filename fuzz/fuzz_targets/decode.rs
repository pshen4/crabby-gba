#![no_main]
use libfuzzer_sys::fuzz_target;
use disasm::thumb;

fuzz_target!(|data: u16| {
    thumb::decode(data);
    // fuzzed code goes here
});
