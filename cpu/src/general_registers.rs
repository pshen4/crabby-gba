mod general_registers_mapping {
    /// This mod provides the following:
    ///   1. get_real_register_index - this returns an appropriate index given an opmode index and register number
    ///   2. get_opmode_map_index - gives you an OpModeIndex from an OpMode - this is mostly for caching.
    ///   3. NUM_REGISTERS_TOTAL - this is the size of the array you need to allocate
    ///
    use crate::op_mode::OpMode;

    pub const NUM_REGISTERS_PER_OPMODE: usize = 16;
    pub const NUM_REGISTERS_TOTAL: usize = 31;
    const SIZEOF_OPMODE_ARRAY: usize = 6;

    #[repr(transparent)]
    #[derive(Debug, Clone, Copy, PartialEq, Eq)]
    pub struct OpModeIndex(usize);

    pub const fn get_opmode_map_index(opmode: OpMode) -> OpModeIndex {
        OpModeIndex(match opmode {
            OpMode::User => 0,
            OpMode::System => 0,
            OpMode::Fiq => 1,
            OpMode::Irq => 2,
            OpMode::Supervisor => 3,
            OpMode::Abort => 4,
            OpMode::Undefined => 5,
        })
    }

    /// Generates a map for the general registers, where:
    ///   - R0-R15 have a unique mapping
    ///   - Every banked register has a unique mapping
    /// The exact mapping is known at compile-time, but is unspecified, and must be gotten through the register map.
    const fn generate_general_register_map() -> [[u8; NUM_REGISTERS_PER_OPMODE]; SIZEOF_OPMODE_ARRAY]
    {
        /// We don't care _what_ the mapping is, just that every banked register has a unique index.
        /// So just assign them indexes in incrementing order
        macro_rules! assign_unique_index {
            ($next_index:ident, $array:expr, $($index:literal) +) => {
                $(
                    $next_index += 1;
                    $array[$index] = $next_index;
                )*
            }
        }
        const DEFAULT_MAP: [u8; NUM_REGISTERS_PER_OPMODE] =
            [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
        let mut register_map = [DEFAULT_MAP; SIZEOF_OPMODE_ARRAY];
        let mut curr_index = 15;
        assign_unique_index!(curr_index, register_map[get_opmode_map_index(OpMode::Fiq).0], 8 9 10 11 12 13 14);
        assign_unique_index!(curr_index, register_map[get_opmode_map_index(OpMode::Irq).0], 13 14);
        assign_unique_index!(curr_index, register_map[get_opmode_map_index(OpMode::Supervisor).0], 13 14);
        assign_unique_index!(curr_index, register_map[get_opmode_map_index(OpMode::Abort).0], 13 14);
        assign_unique_index!(curr_index, register_map[get_opmode_map_index(OpMode::Undefined).0], 13 14);
        register_map
    }
    const REGISTER_MAPPING: [[u8; NUM_REGISTERS_PER_OPMODE]; SIZEOF_OPMODE_ARRAY] =
        generate_general_register_map();

    #[contracts::debug_requires((reg_num as usize) < NUM_REGISTERS_PER_OPMODE)]
    #[contracts::debug_ensures(ret < NUM_REGISTERS_TOTAL)]
    pub fn get_real_register_index(opmode: OpModeIndex, reg_num: u8) -> usize {
        REGISTER_MAPPING[opmode.0][reg_num as usize].into()
    }

    #[cfg(test)]
    mod tests {

        use crate::general_registers::OpMode;

        use super::{get_opmode_map_index, REGISTER_MAPPING};
        #[test]
        fn register_map_has_31_unique_banked_registers() {
            let registers = REGISTER_MAPPING[get_opmode_map_index(OpMode::User).0]
                .iter()
                .chain(REGISTER_MAPPING[get_opmode_map_index(OpMode::Fiq).0][8..=14].iter())
                .chain(REGISTER_MAPPING[get_opmode_map_index(OpMode::Irq).0][13..=14].iter())
                .chain(REGISTER_MAPPING[get_opmode_map_index(OpMode::Supervisor).0][13..=14].iter())
                .chain(REGISTER_MAPPING[get_opmode_map_index(OpMode::Abort).0][13..=14].iter())
                .chain(REGISTER_MAPPING[get_opmode_map_index(OpMode::Undefined).0][13..=14].iter());
            let set: std::collections::HashSet<_> = registers.collect();
            assert_eq!(set.len(), 31);
            assert_eq!(**set.iter().max().unwrap(), 30);
        }

        #[test]
        fn register_map_sys_equals_user() {
            assert_eq!(
                REGISTER_MAPPING[get_opmode_map_index(OpMode::User).0],
                REGISTER_MAPPING[get_opmode_map_index(OpMode::System).0]
            );
        }

        #[test]
        fn register_map_has_right_common_mapping() {
            for mode in [
                get_opmode_map_index(OpMode::Irq).0,
                get_opmode_map_index(OpMode::Supervisor).0,
                get_opmode_map_index(OpMode::Abort).0,
                get_opmode_map_index(OpMode::Undefined).0,
            ] {
                assert_eq!(
                    REGISTER_MAPPING[mode][0..13],
                    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
                );
                assert_eq!(REGISTER_MAPPING[mode as usize][15], 15);
            }
            assert_eq!(
                REGISTER_MAPPING[get_opmode_map_index(OpMode::Fiq).0][0..8],
                [0, 1, 2, 3, 4, 5, 6, 7]
            );
            assert_eq!(
                REGISTER_MAPPING[get_opmode_map_index(OpMode::Fiq).0][15],
                15
            );
        }
    }
}

use crate::op_mode::OpMode;
use general_registers_mapping::{get_opmode_map_index, get_real_register_index, OpModeIndex};
use instructions::primitives::RegisterNum;
use std::ops::Index;
use std::ops::IndexMut;

#[derive(Debug, Clone)]
pub struct GeneralRegisters {
    general: [u32; 31],
    opmode_index: OpModeIndex,
}

impl GeneralRegisters {
    pub fn new(opmode: OpMode) -> Self {
        Self {
            general: [0; 31],
            opmode_index: get_opmode_map_index(opmode),
        }
    }

    pub fn set_opmode(&mut self, opmode: OpMode) {
        self.opmode_index = get_opmode_map_index(opmode);
    }
}

impl Index<RegisterNum> for GeneralRegisters {
    type Output = u32;

    fn index(&self, register: RegisterNum) -> &Self::Output {
        &self.general[get_real_register_index(self.opmode_index, register.0)]
    }
}

impl IndexMut<RegisterNum> for GeneralRegisters {
    fn index_mut(&mut self, register: RegisterNum) -> &mut Self::Output {
        &mut self.general[get_real_register_index(self.opmode_index, register.0)]
    }
}

#[cfg(test)]
mod test {
    use super::{GeneralRegisters, OpMode};
    use instructions::primitives::RegisterNum;

    #[test]
    fn user_sys_are_the_same() {
        let mut reg = GeneralRegisters::new(OpMode::User);
        for i in 0..=15 {
            reg.set_opmode(OpMode::User);
            assert_eq!(reg[RegisterNum(i)], 0);
            reg[RegisterNum(i)] = 1;
            assert_eq!(reg[RegisterNum(i)], 1);
            reg.set_opmode(OpMode::System);
            assert_eq!(reg[RegisterNum(i)], 1);
            reg[RegisterNum(i)] = 0;
            reg.set_opmode(OpMode::User);
            assert_eq!(reg[RegisterNum(i)], 0);
        }
    }

    #[test]
    fn fiq_has_more_differences() {
        let mut reg = GeneralRegisters::new(OpMode::Irq);
        reg[RegisterNum(8)] = 1;
        assert_eq!(reg[RegisterNum(8)], 1);
        reg.set_opmode(OpMode::Fiq);
        assert_eq!(reg[RegisterNum(8)], 0);
    }

    #[test]
    fn irq_fiq_have_some_similarities() {
        let mut reg = GeneralRegisters::new(OpMode::Irq);
        reg[RegisterNum(0)] = 1;
        assert_eq!(reg[RegisterNum(0)], 1);
        reg.set_opmode(OpMode::Fiq);
        assert_eq!(reg[RegisterNum(0)], 1);
    }

    use proptest::prelude::*;
    fn different_opmode() -> BoxedStrategy<OpMode> {
        prop_oneof![
            Just(OpMode::Irq),
            Just(OpMode::Fiq),
            Just(OpMode::Supervisor),
            Just(OpMode::Abort),
            Just(OpMode::Undefined),
        ]
        .boxed()
    }

    #[test]
    #[should_panic]
    fn dont_go_oob() {
        let mut reg = GeneralRegisters::new(OpMode::Irq);
        reg[RegisterNum(16)] = 1;
    }

    fn any_opmode() -> BoxedStrategy<OpMode> {
        prop_oneof![different_opmode(), Just(OpMode::User), Just(OpMode::System),].boxed()
    }
    proptest! {
        #[test]
        fn sp_and_lr_are_different(
            opmode1 in different_opmode(),
            opmode2 in different_opmode()
        ) {
            if opmode1 != opmode2 {
                let mut reg = GeneralRegisters::new(opmode1);
                for i in 13..=14 {
                    reg.set_opmode(opmode1);
                    assert_eq!(reg[RegisterNum(i)], 0);
                    reg[RegisterNum(i)] = 1;
                    assert_eq!(reg[RegisterNum(i)], 1);
                    reg.set_opmode(opmode2);
                    assert_eq!(reg[RegisterNum(i)], 0);
                }
            }
        }

        #[test]
        fn simple_addition(opmode in any_opmode()) {
            let mut reg = GeneralRegisters::new(opmode);
            reg[RegisterNum(2)] = 2;
            reg[RegisterNum(2)] = reg[RegisterNum(2)] + reg[RegisterNum(2)];
            assert_eq!(reg[RegisterNum(2)], 4);
        }
    }
}
