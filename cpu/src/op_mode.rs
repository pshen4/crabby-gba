#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum OpMode {
    User,
    System,
    Fiq,
    Irq,
    Supervisor,
    Abort,
    Undefined,
}

impl Default for OpMode {
    fn default() -> Self {
        OpMode::User
    }
}

impl From<u8> for OpMode {
    fn from(raw: u8) -> OpMode {
        match raw {
            0b10000 => OpMode::User,
            0b10001 => OpMode::Fiq,
            0b10010 => OpMode::Irq,
            0b10011 => OpMode::Supervisor,
            0b10111 => OpMode::Abort,
            0b11011 => OpMode::Undefined,
            0b11111 => OpMode::System,
            _ => unimplemented!(),
        }
    }
}

impl From<OpMode> for u32 {
    fn from(raw: OpMode) -> u32 {
        match raw {
            OpMode::User => 0b10000,
            OpMode::Fiq => 0b10001,
            OpMode::Irq => 0b10010,
            OpMode::Supervisor => 0b10011,
            OpMode::Abort => 0b10111,
            OpMode::Undefined => 0b11011,
            OpMode::System => 0b11111,
        }
    }
}
