use crate::op_mode::OpMode;
use crate::status_register::StatusRegister;
use std::ops::{Deref, DerefMut};

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct SavedStatusRegister {
    spsr: [StatusRegister; 5],
    opmode_index: usize,
}

const fn get_spsr_index(opmode: OpMode) -> usize {
    match opmode {
        OpMode::Fiq => 0,
        OpMode::Irq => 1,
        OpMode::Supervisor => 2,
        OpMode::Abort => 3,
        OpMode::Undefined => 4,
        _ => 5, // this is OOB - User and System don't have an SPSR
    }
}

impl SavedStatusRegister {
    pub fn new(opmode: OpMode) -> Self {
        Self {
            spsr: [StatusRegister::default(); 5],
            opmode_index: get_spsr_index(opmode),
        }
    }

    pub fn set_opmode(&mut self, opmode: OpMode) {
        self.opmode_index = get_spsr_index(opmode);
    }
}

impl Default for SavedStatusRegister {
    fn default() -> Self {
        SavedStatusRegister::new(OpMode::default())
    }
}

impl Deref for SavedStatusRegister {
    type Target = StatusRegister;
    fn deref(&self) -> &Self::Target {
        &self.spsr[self.opmode_index]
    }
}

impl DerefMut for SavedStatusRegister {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.spsr[self.opmode_index]
    }
}

#[cfg(test)]
mod tests {
    use super::SavedStatusRegister;
    use crate::{op_mode::OpMode, spsr::StatusRegister};

    #[test]
    fn unique_registers() {
        let mut spsr = SavedStatusRegister::new(OpMode::Abort);
        for (i, mode) in [
            (0b10001, OpMode::Fiq),
            (0b10010, OpMode::Irq),
            (0b10011, OpMode::Supervisor),
            (0b10111, OpMode::Abort),
            (0b11011, OpMode::Undefined),
        ] {
            spsr.set_opmode(mode);
            assert_eq!(*spsr, StatusRegister::from(0b10000));
            *spsr = StatusRegister::from(i);
            assert_eq!(*spsr, StatusRegister::from(i));
        }
    }

    #[test]
    #[should_panic]
    fn dont_use_this_with_user() {
        let mut spsr = SavedStatusRegister::new(OpMode::User);
        *spsr = StatusRegister::from(1);
    }

    #[test]
    #[should_panic]
    fn dont_use_this_with_system() {
        let mut spsr = SavedStatusRegister::new(OpMode::System);
        *spsr = StatusRegister::from(1);
    }
}
