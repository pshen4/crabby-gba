use crate::op_mode::OpMode;
use std::convert::From;
use util::bits::GetBits;

#[derive(Debug, Clone, Default, Copy, PartialEq, Eq)]
pub struct StatusRegister {
    negative: bool,
    zero: bool,
    carry_borrow_extend: bool,
    overflow: bool,
    irq_disable: bool,
    fiq_disable: bool,
    state_bit: bool,
    mode: OpMode,
}

impl From<u32> for StatusRegister {
    fn from(raw: u32) -> StatusRegister {
        assert_eq!(raw.get_bits::<u32, 27, 8>(), 0);
        StatusRegister {
            negative: raw.test_bit::<31>(),
            zero: raw.test_bit::<30>(),
            carry_borrow_extend: raw.test_bit::<29>(),
            overflow: raw.test_bit::<28>(),
            irq_disable: raw.test_bit::<7>(),
            fiq_disable: raw.test_bit::<6>(),
            state_bit: raw.test_bit::<5>(),
            mode: raw.get_bits::<u8, 4, 0>().into(),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::op_mode::OpMode;
    use std::ops::Fn;
    use test_case::test_case;

    use super::StatusRegister;
    #[test_case(31, |r| { r.negative })]
    #[test_case(30, |r| { r.zero })]
    #[test_case(29, |r| { r.carry_borrow_extend })]
    #[test_case(28, |r| { r.overflow })]
    #[test_case(7, |r| { r.irq_disable })]
    #[test_case(6, |r| { r.fiq_disable })]
    #[test_case(5, |r| { r.state_bit })]
    fn check_bits(i: u8, f: impl Fn(StatusRegister) -> bool) {
        let input: u32 = 0b10000 | (1 << i);
        assert!(f(StatusRegister::from(input)));
    }

    #[test_case(OpMode::User, 0b10000)]
    #[test_case(OpMode::Fiq, 0b10001)]
    #[test_case(OpMode::Irq, 0b10010)]
    #[test_case(OpMode::Supervisor, 0b10011)]
    #[test_case(OpMode::Abort, 0b10111)]
    #[test_case(OpMode::Undefined, 0b11011)]
    #[test_case(OpMode::System, 0b11111)]
    fn check_modes(o: OpMode, mode_bits: u32) {
        assert_eq!(StatusRegister::from(mode_bits).mode, o);
    }
}
