use instructions::primitives::Instruction;
use util::bits::GetBits;

pub fn decode(raw: u16) -> Instruction {
    match raw.get_bits::<u16, 15, 13>() {
        0b000 => {
            if raw.get_bits::<u16, 12, 11>() != 0b11_u16 {
                Instruction::MoveShiftedRegister(raw.into())
            } else {
                Instruction::AddSubtract(raw.into())
            }
        }
        0b001 => Instruction::OpImmediate(raw.into()),
        0b010 => {
            match raw.get_bits::<u16, 12, 10>() {
                0b000 => return Instruction::AluOperations(raw.into()),
                0b001 => return Instruction::HiOp(raw.into()),
                _ => {}
            }

            if raw.get_bits::<u16, 12, 11>() == 0b01 {
                return Instruction::PcRelativeLoad(raw.into());
            }

            let bit_12 = raw.test_bit::<12>();
            let bit_9 = raw.test_bit::<9>();

            if bit_12 && !bit_9 {
                return Instruction::LoadStoreReg(raw.into());
            } else if bit_12 && bit_9 {
                return Instruction::LoadStoreSignExtended(raw.into());
            }

            unreachable!("no match found in 0b010_thumb");
        }
        0b011 => Instruction::LoadStoreImmediateOffset(raw.into()),
        0b100 => {
            if !raw.test_bit::<12>() {
                Instruction::LoadStoreHalfword(raw.into())
            } else {
                Instruction::SpRelativeLoadStore(raw.into())
            }
        }
        0b101 => {
            if !raw.test_bit::<12>() {
                return Instruction::LoadAddress(raw.into());
            }

            if !raw.test_bit::<10>() {
                Instruction::OffsetStackPointer(raw.into())
            } else {
                Instruction::PushPopRegisters(raw.into())
            }
        }
        0b110 => {
            if !raw.test_bit::<12>() {
                Instruction::MultipleLoadStore(raw.into())
            } else if raw.get_bits::<u16, 11, 8>() != 0b1111 {
                Instruction::ConditionalBranch(raw.into())
            } else {
                Instruction::SoftwareInterrupt(raw.into())
            }
        }
        0b111 => {
            if !raw.test_bit::<12>() {
                Instruction::UnconditionalBranch(raw.into())
            } else {
                Instruction::LongBranchWithLink(raw.into())
            }
        }
        _ => unreachable!(),
    }
}

#[cfg(test)]
mod test {
    use super::{decode, Instruction};

    #[test]
    fn decode_msr() {
        assert!(matches!(
            decode(0 as u16),
            Instruction::MoveShiftedRegister(_)
        ))
    }

    #[test]
    fn decode_add_subtract() {
        assert!(matches!(decode(0b11 << 11u16), Instruction::AddSubtract(_)))
    }

    #[test]
    fn decode_op_immediate() {
        assert!(matches!(
            decode(0b001 << 13u16),
            Instruction::OpImmediate(_)
        ));
    }

    #[test]
    fn decode_alu_match() {
        assert!(matches!(decode(1 << 14u16), Instruction::AluOperations(_)));
    }

    #[test]
    fn decode_alu_close_match() {
        assert!(!matches!(
            decode((1 << 14u16) | (1 << 10u16)),
            Instruction::AluOperations(_)
        ));
        assert!(!matches!(
            decode((1 << 14u16) | (1 << 11u16)),
            Instruction::AluOperations(_)
        ));
        assert!(!matches!(
            decode((1 << 14u16) | (1 << 12u16)),
            Instruction::AluOperations(_)
        ));
        assert!(!matches!(
            decode((1 << 14u16) | (1 << 12u16) | (1 << 9u16)),
            Instruction::AluOperations(_)
        ));
    }

    #[test]
    fn decode_hiop() {
        assert!(matches!(decode(0b10001 << 10u16), Instruction::HiOp(_)));
    }

    #[test]
    fn decode_pcrl() {
        assert!(matches!(
            decode(0b01001 << 11),
            Instruction::PcRelativeLoad(_)
        ));
    }

    #[test]
    fn decode_load_store_reg() {
        let input = (0b010 << 13) | (1 << 12) as u16;
        assert!(matches!(decode(input), Instruction::LoadStoreReg(_)));
    }

    #[test]
    fn decode_load_store_sign_extended() {
        let input = (0b010 << 13) | (1 << 12) | (1 << 9) as u16;
        assert!(matches!(
            decode(input),
            Instruction::LoadStoreSignExtended(_)
        ));
    }

    #[test]
    fn decode_ls_immediate_offset() {
        assert!(matches!(
            decode(0b011 << 13 as u16),
            Instruction::LoadStoreImmediateOffset(_)
        ))
    }

    #[test]
    fn decode_ls_halfword() {
        assert!(matches!(
            decode(0b100 << 13),
            Instruction::LoadStoreHalfword(_)
        ))
    }

    #[test]
    fn decode_sp_relative_ls() {
        assert!(matches!(
            decode((0b100 << 13) | (1 << 12)),
            Instruction::SpRelativeLoadStore(_)
        ))
    }

    #[test]
    fn decode_load_address() {
        assert!(matches!(decode(0b101 << 13), Instruction::LoadAddress(_)))
    }

    #[test]
    fn decode_offset_sp() {
        assert!(matches!(
            decode((0b101 << 13) | (1 << 12) as u16),
            Instruction::OffsetStackPointer(_)
        ))
    }

    #[test]
    fn decode_push_pop_regs() {
        assert!(matches!(
            decode((0b101 << 13) | (1 << 12) | (1 << 10) as u16),
            Instruction::PushPopRegisters(_)
        ))
    }

    #[test]
    fn decode_multiple_ls() {
        assert!(matches!(
            decode(0b110 << 13),
            Instruction::MultipleLoadStore(_)
        ))
    }

    #[test]
    fn decode_cb() {
        assert!(matches!(
            decode((0b110 << 13) | (1 << 12) as u16),
            Instruction::ConditionalBranch(_)
        ))
    }

    #[test]
    fn decode_swi() {
        assert!(matches!(
            decode((0b110 << 13) | (1 << 12) | (0b1111 << 8) as u16),
            Instruction::SoftwareInterrupt(_)
        ))
    }

    #[test]
    fn decode_ucb() {
        assert!(matches!(
            decode(0b111 << 13),
            Instruction::UnconditionalBranch(_)
        ))
    }

    #[test]
    fn decode_long_link_with_branch() {
        assert!(matches!(
            decode((0b111 << 13) | (1 << 12) as u16),
            Instruction::LongBranchWithLink(_)
        ))
    }
}
