use std::ops::{BitAnd, Shr};

pub trait IntoOrTruncate<I> {
    fn into_or_truncate(self) -> I;
}

impl<T> IntoOrTruncate<T> for T {
    #[inline(always)]
    fn into_or_truncate(self) -> T {
        self
    }
}

impl IntoOrTruncate<u8> for u16 {
    #[inline(always)]
    fn into_or_truncate(self) -> u8 {
        self as u8
    }
}

impl IntoOrTruncate<u8> for u32 {
    #[inline(always)]
    fn into_or_truncate(self) -> u8 {
        self as u8
    }
}

impl IntoOrTruncate<u16> for u32 {
    #[inline(always)]
    fn into_or_truncate(self) -> u16 {
        self as u16
    }
}

pub trait GetBits
where
    Self: Shr<u8>,
    Self: Sized,
    u8: Into<Self>,
    <Self as Shr<u8>>::Output: BitAnd<Self>,
    // This must be implemented to support get_bit
    <<Self as Shr<u8>>::Output as BitAnd<Self>>::Output: IntoOrTruncate<u8>,
{
    #[inline(always)]
    fn get_bit<const I: u8>(self) -> u8 {
        self.get_bits::<u8, I, I>()
    }

    #[inline(always)]
    fn test_bit<const I: u8>(self) -> bool {
        self.get_bit::<I>() == 1
    }

    fn get_bits<O, const S: u8, const E: u8>(self) -> O
    where
        O: Sized,
        <<Self as Shr<u8>>::Output as BitAnd<Self>>::Output: IntoOrTruncate<O>;
}

impl GetBits for u8 {
    #[inline(always)]
    fn get_bits<O, const S: u8, const E: u8>(self) -> O
    where
        O: Sized,
        <<Self as Shr<u8>>::Output as BitAnd<Self>>::Output: IntoOrTruncate<O>,
    {
        assert!(S >= E);
        let shr = self >> E;
        let num_bits: u8 = S + 1 - E;
        assert!(num_bits <= ((8 * std::mem::size_of::<O>()) as u8));
        let mask = (1u8 << num_bits) - 1;
        (shr & mask).into_or_truncate()
    }
}

impl GetBits for u16 {
    #[inline(always)]
    fn get_bits<O, const S: u8, const E: u8>(self) -> O
    where
        O: Sized,
        <<Self as Shr<u8>>::Output as BitAnd<Self>>::Output: IntoOrTruncate<O>,
    {
        assert!(S >= E);
        let shr = self >> E;
        let num_bits: u8 = S + 1 - E;
        assert!(num_bits <= ((8 * std::mem::size_of::<O>()) as u8));
        let mask = (Self::from(1u8) << num_bits) - 1;
        (shr & mask).into_or_truncate()
    }
}

impl GetBits for u32 {
    #[inline(always)]
    fn get_bits<O, const S: u8, const E: u8>(self) -> O
    where
        O: Sized,
        <<Self as Shr<u8>>::Output as BitAnd<Self>>::Output: IntoOrTruncate<O>,
    {
        assert!(S >= E);
        let shr = self >> E;
        let num_bits: u8 = S + 1 - E;
        assert!(num_bits <= ((8 * std::mem::size_of::<O>()) as u8));
        let mask = (Self::from(1u8) << num_bits) - 1;
        (shr & mask).into_or_truncate()
    }
}

#[test]
fn foo() {
    assert_eq!((0b010100 as u16).get_bits::<u8, 2, 0>(), 0b100 as u8);
    assert_eq!((0b010100 as u16).get_bits::<u16, 2, 0>(), 0b100 as u16);
}
